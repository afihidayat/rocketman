extends Node2D
const scn_pipe = preload("res://pipe.tscn")
const PIPE_WIDTH = 26
const OFFSET_Y = 55
const OFFSET_X = 80
const GROUND_HEIGHT = 55
const AMOUNT_TO_FILL_VIEW = 3

func _ready():
	var bird = get_node("../bird")
	if bird:
		bird.connect("state_changed",self, "_on_bird_state_changed", [], CONNECT_ONESHOT)
	pass

func _on_bird_state_changed(bird):
	if bird.get_state() == bird.STATE_FLAPPING:
		start()
	pass

func start():
	go_init_pos()
	for i in range(AMOUNT_TO_FILL_VIEW):
		spawn_and_move()
		go_next_position()
		print("AMOUNT_TO_FILL_VIEW")
	pass
	

func go_init_pos():
	randomize()
	var init_pos = Vector2()
	var viewportx = get_viewport().size.x
	var viewporty = get_viewport().size.y
	init_pos.x = viewportx+  PIPE_WIDTH/2
	#print(init_pos.x, "mulai")
	var camera = get_node("../camera")
	if camera:
		init_pos.x += (camera.get_total_pos().x*2)
	init_pos.y = rand_range(0+OFFSET_Y, viewporty-GROUND_HEIGHT-OFFSET_Y)
	set_position(init_pos)
	pass

func spawn_pipe():
	var new_pipe = scn_pipe.instance()
	new_pipe.set_position(get_position())
	#print(get_position())
	new_pipe.connect("destroyed", self, "spawn_and_move")
	new_pipe.connect("destroyed", self, "go_next_position")
	get_node("container").add_child(new_pipe)
	pass

func spawn_and_move():
	spawn_pipe()
	go_next_position()
	pass

func go_next_position():
	randomize()
	var next_pos = get_position()
	var viewportx = get_viewport().size.x
	var viewporty = get_viewport().size.y
	next_pos.x +=OFFSET_X
	next_pos.y = rand_range(0+OFFSET_Y, viewporty-GROUND_HEIGHT-OFFSET_Y)
	set_position(next_pos)
	pass