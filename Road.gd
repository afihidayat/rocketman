extends StaticBody2D

onready var camera = get_tree().get_root().get_node("Game/camera")

signal destroyed

onready var bottom_right = get_node("bottom_right")
func _ready():
	add_to_group(game.GROUP_GROUNDS)
	pass

func _process(delta):
	if camera == null:return
	if bottom_right.get_global_position().x <= camera.get_total_pos().x:
		queue_free()
		emit_signal("destroyed")